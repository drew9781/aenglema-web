
FROM golang:1.16-buster as builder
ENV APP_USER app
ENV APP_HOME /go/src/aenglema-web
RUN groupadd $APP_USER && useradd -m -g $APP_USER -l $APP_USER
RUN mkdir -p $APP_HOME && chown -R $APP_USER:$APP_USER $APP_HOME
WORKDIR $APP_HOME
USER $APP_USER
COPY src/ .
RUN go mod download\
&& go mod verify \
&& go build -o aenglema-web
FROM golang:1.16-buster
ENV APP_USER app
ENV APP_HOME /go/src/aenglema-web
RUN groupadd $APP_USER && useradd -m -g $APP_USER -l $APP_USER
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
COPY src .
COPY --from=builder $APP_HOME/aenglema-web $APP_HOME/src/
RUN chown -R ${APP_USER}:${APP_USER} ${APP_HOME}
EXPOSE 80
USER $APP_USER
CMD ["./src/aenglema-web"]