package controllers

import (
	beego "github.com/beego/beego/v2/server/web"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.TplName = "index-onepage-dark.html"
}

type BlogOneController struct {
	beego.Controller
}

func (c *BlogOneController) Get() {
	c.TplName = "blog-post-1.html"
}

type BlogTwoController struct {
	beego.Controller
}

func (c *BlogTwoController) Get() {
	c.TplName = "blog-post-2.html"
}

type RecipeeChickenTzatzikiController struct {
	beego.Controller
}

func (c *RecipeeChickenTzatzikiController) Get() {
	c.TplName = "recipee-chicken-tzatziki.html"
}