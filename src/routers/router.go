package routers

import (
	"aenglema-web/controllers"
	beego "github.com/beego/beego/v2/server/web"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/blogpostone", &controllers.BlogOneController{})
    beego.Router("/blogposttwo", &controllers.BlogTwoController{})
    beego.Router("/recipee-chicken-tzatziki", &controllers.RecipeeChickenTzatzikiController{})
}
